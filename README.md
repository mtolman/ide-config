# Configuration for IDEs that I use

This repo serves as a starting point for myself when setting up IDEs.

I have configuration for the different IDEs that I use saved here.

Feel free to use, modify, distribute, etc these configurations.
