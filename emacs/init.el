(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "Your version of Emacs does not support SSL connections,
    which is unsafe because it allows man-in-the-middle attacks.
    There are two things you can do about this warning:
    1. Install an Emacs version that does support SSL and be safe.
    2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)
;(package-refresh-contents)

(defvar my-packages '(better-defaults
                      projectile
                      cider
                      clojure-mode
                      indium
                      ivy
                      neotree
                      ztree
                      undo-tree
                      rainbow-delimiters
                      visual-regexp
                      multiple-cursors
                      fix-word
                      browse-kill-ring
                      smartparens
                      company
                      company-erlang
                      company-nginx
                      company-math
                      company-emoji
                      paredit
                      emmet-mode
                      tide
                      nodejs-repl
                      nord-theme
                      erlang
                      tuareg
                      elm-mode
                      guide-key
                      multi-term
                      emojify
                      alda-mode
                      magit
                      minesweeper
                      xkcd
                      pacmacs
                      material-theme
                      js2-mode
                      js2-refactor
                      docker
                      ledger-mode
                      xref-js2
		      discover-js2-refactor
		      material-theme
		      ng2-mode
		      pacmacs
		      minesweeper
		      multi-term
		      guide-key
		      elm-mode
		      tuareg
		      tide
		      emmet-mode
		      company-emoji
		      fix-word
		      visual-regexp
		      rainbow-delimiters
		      undo-tree
		      hydra))
 
(dolist (p my-packages)
  (unless (package-installed-p p)
    (package-install p)))
 
 
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
 
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)
;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
 
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
 
;(add-hook 'after-init-hook #'global-emojify-mode)
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "82358261c32ebedfee2ca0f87299f74008a2e5ba5c502bde7aaa15db20ee3731" default)))
 '(global-whitespace-mode t)
 '(package-selected-packages
   (quote
    (ipcalc sudoku slime-volleyball nord-theme nodejs-repl magit 2048-game xref-js2 discover-js2-refactor material-theme pacmacs xkcd minesweeper emojify multi-term guide-key elm-mode tuareg tide emmet-mode paredit company-emoji company-math company-nginx company-erlang smartparens browse-kill-ring fix-word visual-regexp rainbow-delimiters undo-tree hydra ztree neotree ivy indium cider projectile better-defaults)))
 '(standard-indent 2)
 '(undo-tree-visualizer-diff t)
 '(whitespace-line-column 120))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'guide-key)
(setq guide-key/guide-key-sequence '("C-x" "C-c"))
(setq guide-key/recursive-key-sequence-flag t)
(guide-key-mode 1)
 
(require 'undo-tree)
                                        ;(add-hook 'after-init-hook 'undo-tree-visualizer-toggle-diff)
 
(require 'neotree)
(add-hook 'after-init-hook 'neotree)
 
 
(autoload 'enable-paredit-mode "paredit"
  "Turn on pseudo-structural editing of Lisp code."
 t)
(add-hook 'emacs-lisp-mode-hook       'enable-paredit-mode)
(add-hook 'lisp-mode-hook             'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(add-hook 'scheme-mode-hook           'enable-paredit-mode)
(add-hook 'clojure-mode-hook          'enable-paredit-mode)

(add-hook 'after-init-hook 'global-company-mode)
 
(global-set-key (kbd "C-@") 'company-complete)
(global-set-key (kbd "C-x C-x") 'set-mark-command)
(global-set-key (kbd "C-x C-e") 'exchange-point-and-mark)
(require 'rainbow-delimiters)
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)

(rainbow-delimiters-mode 1)
(paredit-mode 1)
(linum-mode 1)
(global-linum-mode 1)

(load-theme 'nord t)
 
(defun format-code ()
  "Formats the current file"
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max))
    (delete-trailing-whitespace)
    (untabify (point-min) (point-max))))
 
(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <right>") 'windmove-right)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x u") 'undo-tree-visualize)
(global-set-key (kbd "C-x C-o") 'find-file)
(global-set-key (kbd "C-x p") 'enable-paredit-mode)
(global-set-key (kbd "C-x M-p") 'disable-paredit-mode)
(global-set-key (kbd "C-M-l") 'format-code)
(global-set-key (kbd "C-x e") 'eval-buffer)
(global-set-key (kbd "C-x C-f") 'find-name-dired)
(global-set-key (kbd "C-x M-f") 'find-grep-dired)
 
(add-hook 'after-init-hook 'global-whitespace-mode)
(add-hook 'after-init-hook 'undo-tree-mode)
 
(setq neo-window-fixed-size nil)
(setq neo-window-width 50)
(setq tab-width 2)
(setq js-indent-level 2)
(setq inhibit-startup-screen t)
(setq-default indent-tabs-mode nil)
 
(require 'js2-refactor)
(require 'xref-js2)
 
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
(js2r-add-keybindings-with-modifier "C-c C-r ")
(define-key js-mode-map (kbd "M-.") nil)
 
(add-hook 'js2-mode-hook (lambda ()
                           (add-hook 'xref-backend-functions #'xref-js2-xrefbackend nil t)))
 
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-x g") 'magit-status)


(setq js2-strict-missing-semi-warning nil)
(setq js2-missing-semi-one-line-override nil)

(show-paren-mode t)
(setq show-paren-style 'parenthesis)


