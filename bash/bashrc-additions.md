For this, simply add the following to your existing ~/.bashrc configuration

```
# Changes bash prompt
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1) /'
}

PS1="\[\033[01;34m\]\w\[\033[00m\]\$ "
PS1="\`if [ \$? = 0 ]; then echo \[\e[32m\]✔\[\e[0m\]; else echo \[\e[31m\]✖\[\e[0m\]; fi\` \[\033[01;34m\]\W\[\033[00m\]\$(parse_git_branch)$ "


# aliases I use
alias em='emacs --no-window-system'
alias ll='ls -alF'
alias la='ls -A'

# Allows pushing a branch to all remotes in one command
# Will also only run pre-push hooks once
alias gpall='git remote | head -n 1 | xargs -L1 -I R git push R && git remote | tail -n +2 | xargs -L1 -I R git push --no-verify R'

alias gdiffm="git log -n 1 master | head -n 1 | awk '{print \$2}'"
alias grevise="git rebase -i \$(gdiffm)"

# For keeping local repos clean after many PR reviews
gitClean () {
    remote=$(git remote -v | head -n 1 | awk '{print $1}');
    head=$(git remote show $remote | grep 'HEAD branch' | cut -d' ' -f5);
    for k in $(git branch | sed /\*/d); do
        if [ -z "$(git log -1 --since='$1 weeks ago' -s $k)" ]; then
            if [ "$head" != "$k" ]; then
                git branch -D $k;
            fi
        fi
    done
}

alias gclean="gitClean"

conv-mov-gif () {
    ffmpeg -i "$1" -pix_fmt rgb8 -r 10 "$2" && gifsicle -O1 "$2" -o "$2"
}

eval "$(zoxide init bash --cmd cd)"
```
